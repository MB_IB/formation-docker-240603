#include <unistd.h>
#include <sys/syscall.h>
void main() {
  syscall(SYS_write, 1, "Hello !", 8);
  syscall(SYS_exit, 0);
}
